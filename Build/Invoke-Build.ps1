
# For use in GitLab CI/CD

$ErrorActionPreference = 'Stop'

$InformationPreference = "Continue"

$parentPath = Split-Path -Path $PSScriptRoot
$moduleName = Get-ChildItem -Path $parentPath -Recurse -Include *.psd1 |
Select-Object -First 1 -ExpandProperty BaseName
$pathSeperator = [IO.Path]::DirectorySeparatorChar
$parentFolder = Split-Path -Path $PSScriptRoot
$modulePath = "$parentFolder$($pathSeperator)$moduleName$($pathSeperator)$moduleName.psd1"

$testAndBuildToolsPath = Resolve-Path -Path "$parentFolder$($pathSeperator)TestAAndBuildTools$($pathSeperator)TestAAndBuildTools.psm1" |
Select-Object -ExpandProperty Path
$moduleManifest = Get-ChildItem -Path $parentPath -Recurse -Include *.psd1 |
Select-Object -First 1

# Install requireed modules
Import-Module $testAndBuildToolsPath -Force
Install-RCRequiredModules -ModuleManifestPath $moduleManifest

# Install Microsoft.PowerShell.PSResourceGet
$null = Install-Module -Name Microsoft.PowerShell.PSResourceGet -Force


# Create credential for package publish
# $User = "psgallery-token"
# $SecureString = ConvertTo-SecureString -String $env:psgallery_token -AsPlainText -Force
# [pscredential]$CredObject = New-Object System.Management.Automation.PSCredential ($User, $SecureString)

# Publish package to PS Gallery
Publish-PSResource -Path $modulePath -Repository PSGallery -SkipDependenciesCheck -ApiKey $env:psgallery_token

