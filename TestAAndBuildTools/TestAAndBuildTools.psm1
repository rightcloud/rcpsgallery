Function Convert-JUnitResult {
    [cmdletbinding()]
    param(
        [string]
        $Path
    )
    [xml]$xml = Get-Content -Path $Path
    $xml.testsuites.testsuite.testcase.Where({$_.status -eq "Failed"}) |
    ForEach-Object {
        $_.failure."#text" = $_.failure.Message
    }
    $xml.Save($Path)
}

Function Install-RCRequiredModules {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [string]
        $ModuleManifestPath
    )
    Import-PowershellDataFile -Path $ModuleManifestPath |
    Where-Object {$_.RequiredModules} |
    Select-Object -ExpandProperty RequiredModules |
    ForEach-Object {
        If ($_ -is 'System.Collections.Hashtable') {
            $_.ModuleName
        } else {
            $_
        }
    } |
    Where-Object { -not (Get-Module -ListAvailable -Name $_ )} |
    ForEach-Object { Install-Module -Name $_ -Force }
}


Function Expand-URLRedirect {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [uri]
        $Uri
    )
    $ErrorActionPreferenceTemp = $ErrorActionPreference
    $ErrorActionPreference = 'SilentlyContinue'
    Invoke-WebRequest -Uri $Uri -MaximumRedirection 0 -SkipHttpErrorCheck |
    Select-Object -ExpandProperty Headers |
    ForEach-Object {$_["Location"]}
    $ErrorActionPreference = $ErrorActionPreferenceTemp
}

Function Get-RCPSGalleryPackageRegistryUri {
    Expand-URLRedirect -Uri http://psgallery.rightcloud.info
}
Function Register-RCPSRepository {
    [CmdletBinding()]
    param(

    )
    # Install Microsoft.PowerShell.PSResourceGet if not installed
    "Microsoft.PowerShell.PSResourceGet" |
    Where-Object { -not (Get-Module -ListAvailable -Name $_ )} |
    ForEach-Object { Install-Module -Name $_ -Force }

    # Get the Repo URI
    $rcGalleryURI= Get-RCPSGalleryPackageRegistryUri
    $rcGalleryRegistred = Get-PSResourceRepository | Where-Object {$_.Uri -eq $rcGalleryURI}
    # Register package repository if not registred
    If (-not ($rcGalleryRegistred)) {
        Register-PSResourceRepository -Name RCPSGallery -Uri $rcGalleryURI -Trusted
        $rcGalleryRegistred = Get-PSResourceRepository | Where-Object {$_.Uri -eq $rcGalleryURI}
    }
    Return $rcGalleryRegistred
}


Function Install-RCPSResource {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [string]
        $Name
    )
    $rcGalleryRegistred = Register-RCPSRepository
    Install-PSResource -Name $Name -Repository $rcGalleryRegistred.Name
}
