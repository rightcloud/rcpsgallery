# Find and create module name and path
$parentPath = Split-Path -Path $PSScriptRoot
$pathSeperator = [IO.Path]::DirectorySeparatorChar
$parentFolder = Split-Path -Path $PSScriptRoot
$testResultOutputPath = "$parentFolder$($pathSeperator)Test.xml"
$testAndBuildToolsPath = Resolve-Path -Path "$parentPath$($pathSeperator)TestAAndBuildTools$($pathSeperator)TestAAndBuildTools.psm1" |
Select-Object -ExpandProperty Path
$moduleManifest = Get-ChildItem -Path $parentPath -Recurse -Include *.psd1 |
Select-Object -First 1
$moduleName = $moduleManifest.BaseName

# Install requireed modules
Import-Module $testAndBuildToolsPath -Force
Install-RCRequiredModules -ModuleManifestPath $moduleManifest

# Create a Pester configuration object using `New-PesterConfiguration`
$config = New-PesterConfiguration

$config.TestResult.Enabled = $true
$config.TestResult.OutputPath = $testResultOutputPath
$config.TestResult.OutputFormat = "JUnitXml"

# Set the test path to specify where your tests are located. In this example, we set the path to the current directory. Pester will look into all subdirectories.
$config.Run.Path = "."
$config.Run.Throw = $true

# Enable Code Coverage
$config.CodeCoverage.Enabled = $true
$config.CodeCoverage.Path = "$PSScriptRoot$($pathSeperator)..$($pathSeperator)$moduleName"
$config.CodeCoverage.OutputPath = "$PSScriptRoot$($pathSeperator)..$($pathSeperator)coverage.xml"
#$config.Output.Verbosity = "Detailed"

# Run Pester tests using the configuration you've created
try {
    Invoke-Pester -Configuration $config
}
catch {
    Convert-JUnitResult -Path $testResultOutputPath
    throw $_
}
