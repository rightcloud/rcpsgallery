
Describe 'PSScriptAnalyzer Default rules' {
    # Need to install/load the module here to create the test cases.
    # This is the only place that execute in Pester Discover phase.
    If (-not (Get-Module -Name PSScriptAnalyzer -ListAvailable)) {
        Install-Module PSScriptAnalyzer -Force
    }
    Import-Module PSScriptAnalyzer -Force

    $parentPath = Split-Path -Path $PSScriptRoot
    $moduleName = Get-ChildItem -Path $parentPath -Recurse -Include *.psd1 |
    Select-Object -First 1 -ExpandProperty BaseName
    $pathSeperator = [IO.Path]::DirectorySeparatorChar
    $modulePath = "$PSScriptRoot$($pathSeperator)..$($pathSeperator)$moduleName"

    $testCases = Foreach ($r in (Get-ScriptAnalyzerRule)) {
        @{
            IncludeRule = $r.RuleName
            Path        = $modulePath
        }
    }

    It "Code should not violate rule: <IncludeRule>" -ForEach $testCases {
        param(
            $IncludeRule,
            $Path
        )
        $ruleResult = Invoke-ScriptAnalyzer -Path $Path -IncludeRule $IncludeRule
        If ( $ruleResult ) {
            throw $ruleResult.Message
        }

    }
}
