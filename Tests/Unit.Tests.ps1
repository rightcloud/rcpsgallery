BeforeAll {
    If (-not (Get-Module -Name Az.Resources -ListAvailable)) {
        Install-Module Az.Resources -Force
    }
    $parentPath = Split-Path -Path $PSScriptRoot
    $moduleName = Get-ChildItem -Path $parentPath -Recurse -Include *.psd1 |
    Select-Object -First 1 -ExpandProperty BaseName
    $pathSeperator = [IO.Path]::DirectorySeparatorChar
    $modulePath = "$PSScriptRoot$($pathSeperator)..$($pathSeperator)$moduleName"
    Import-Module $modulePath -Force
}



Describe 'Get-Help for all cmdlets, to provoke parsing errors' {

    $parentPath = Split-Path -Path $PSScriptRoot
    $moduleName = Get-ChildItem -Path $parentPath -Recurse -Include *.psd1 |
    Select-Object -First 1 -ExpandProperty BaseName
    $pathSeperator = [IO.Path]::DirectorySeparatorChar
    $modulePath = "$PSScriptRoot$($pathSeperator)..$($pathSeperator)$moduleName"
    Import-Module $modulePath -Force

    $testCases = Get-Command -Module $moduleName -ErrorAction Stop |
    Select-Object -ExpandProperty Name |
    ForEach-Object {
        @{
            CmdLet = $_
        }
    }
    It "Should be able to Get-Help for cmdlet: <CmdLet>" -ForEach $testCases {
        param(
            $CmdLet
        )
        try {
            $null = Get-Help -Name $CmdLet -ErrorAction Stop
        }
        catch {
            throw $_
        }

    }
}