Describe 'PSScriptAnalyzer Injection Hunter rules' {
    # Need to install/load the module here to create the test cases.
    # This is the only place that execute in Pester Discover phase.
    If (-not (Get-Module -Name PSScriptAnalyzer -ListAvailable)) {
        Install-Module PSScriptAnalyzer -Force
    }
    Import-Module PSScriptAnalyzer -Force

    If (-not (Get-Module -Name InjectionHunter -ListAvailable)) {
        Install-Module InjectionHunter -Force
    }
    Import-Module InjectionHunter -Force
    $injectionHunter = Get-Module InjectionHunter -ListAvailable | Select-Object -First 1 | Select-Object -ExpandProperty Path

    $parentPath = Split-Path -Path $PSScriptRoot
    $moduleName = Get-ChildItem -Path $parentPath -Recurse -Include *.psd1 |
    Select-Object -First 1 -ExpandProperty BaseName
    $pathSeperator = [IO.Path]::DirectorySeparatorChar
    $modulePath = "$PSScriptRoot$($pathSeperator)..$($pathSeperator)$moduleName"

    $testCases = Foreach ($r in (Get-ScriptAnalyzerRule -CustomRulePath $injectionHunter )) {
        @{
            IncludeRule = $r.RuleName
            Path        = $modulePath
            CustomRulePath = $injectionHunter
        }
    }

    It "Code should not violate rule: <IncludeRule>" -ForEach $testCases {
        param(
            $IncludeRule,
            $Path,
            $CustomRulePath
        )
        $ruleResult = Invoke-ScriptAnalyzer -Path $Path -IncludeRule $IncludeRule -CustomRulePath $CustomRulePath
        If ( $ruleResult ) {
            throw $ruleResult.Message
        }

    }
}

